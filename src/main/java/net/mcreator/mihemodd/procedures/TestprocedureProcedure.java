package net.mcreator.mihemodd.procedures;

public class TestprocedureProcedure {

	@Mod.EventBusSubscriber
	private static class GlobalTrigger {
		@SubscribeEvent
		public static void onBonemeal(BonemealEvent event) {
			PlayerEntity entity = event.getPlayer();
			double i = event.getPos().getX();
			double j = event.getPos().getY();
			double k = event.getPos().getZ();
			World world = event.getWorld();
			ItemStack itemstack = event.getStack();
			Map<String, Object> dependencies = new HashMap<>();
			dependencies.put("x", i);
			dependencies.put("y", j);
			dependencies.put("z", k);
			dependencies.put("world", world);
			dependencies.put("itemstack", itemstack);
			dependencies.put("entity", entity);
			dependencies.put("blockstate", event.getBlock());
			dependencies.put("event", event);
			executeProcedure(dependencies);
		}
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				MihemoddMod.LOGGER.warn("Failed to load dependency entity for procedure Testprocedure!");
			return;
		}

		Entity entity = (Entity) dependencies.get("entity");

		if (!entity.world.isRemote())
			entity.remove();
	}

}
